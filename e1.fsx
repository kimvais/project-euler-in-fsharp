﻿let isDivisibleBy3or5 it =
    it % 3 = 0 || it % 5 = 0

[1..999] |> Seq.filter isDivisibleBy3or5 |> Seq.sum |> printfn "%d"
