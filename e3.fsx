﻿
let findDivisors (x: int64) =
    let maxFactor = float >> sqrt >> int64 <| x
    let isDivisibleBy (y: int64) =
        x % y = 0L
    [2L..maxFactor] |> Seq.filter isDivisibleBy

let isPrime s =
    s |> findDivisors |> Seq.isEmpty
    
    
findDivisors 13195L |> Seq.filter isPrime |> printfn "%A"
findDivisors 600851475143L |> Seq.filter isPrime |> Seq.max |> printfn "%d"
