﻿let findDivisors (x: int64) =
    let maxFactor = float >> sqrt >> int64 <| x
    let isDivisibleBy (y: int64) =
        x % y = 0L
    [2L..maxFactor] |> Seq.filter isDivisibleBy

let isPrime s =
    s |> findDivisors |> Seq.isEmpty

let addOne x = x |> int64 |> (+) 1L

let candidates = Seq.initInfinite addOne

candidates |> Seq.filter isPrime |> Seq.take 10002 |> Seq.last |> printfn "%d"

