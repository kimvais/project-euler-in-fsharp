﻿
let isEven it =
    it % 2 = 0
let fib = Seq.unfold (fun (a, b) -> Some(a + b, (a + b, a))) (0, 1)

fib|> Seq.takeWhile (fun x -> x <= 4000000) |> Seq.filter isEven |> Seq.sum
    |> printfn "%d"

