// I cheated and took this from https://stackoverflow.com/a/4631738/180174
let primes = 
    let rec nextPrime n p primes =
        if primes |> Map.containsKey n then
            nextPrime (n + p) p primes
        else
            primes.Add(n, p)

    let rec prime n primes =
        seq {
            if primes |> Map.containsKey n then
                let p = primes.Item n
                yield! prime (n + 1L) (nextPrime (n + p) p (primes.Remove n))
            else
                yield n
                yield! prime (n + 1L) (primes.Add(n * n, n))
        }

    prime 2L Map.empty
    
    
primes |> Seq.takeWhile (fun x -> x < 2000000L) |> Seq.sum |> printfn "%d"