let pow2 x = x * x


let sumOfSquares x =
        x |> Seq.map pow2 |> Seq.sum
        
let squareOfSums x =
        x |> Seq.sum |> pow2
        
let diff x = -(sumOfSquares x) + (squareOfSums x)

printfn "%d" <| diff [1..10]
printfn "%d" <| diff [1..100]
        
