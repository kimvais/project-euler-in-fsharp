let getProducts = seq {
    for x in [ 100..999 ] do
        for y in [ 100..999 ] do
            yield x * y
    }

let isPalindrome x =
    let fwd = x |> string
    let bck (x: string) = x |> Seq.rev |> System.String.Concat
    fwd = bck fwd

getProducts |> Seq.filter isPalindrome |> Seq.max
    |> printfn "%d"
