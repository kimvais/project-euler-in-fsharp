let pow2 x = x * x

let triplet = seq {
    for c in [ 1L..998L ] do
        for b in [ 1L..(c - 1L) ] do
            for a in [ 1L..(b - 1L) ] do
                if (pow2 a) + (pow2 b) = (pow2 c) then
                    yield (a, b, c)
 }

let sumIsThousand (a, b, c) =
    let sum = a + b + c
    let cond = sum = 1000L
    if cond then printfn "%d %d %d" a b c
    cond

let calcProduct (a, b, c) =
    a * b * c

triplet|> Seq.filter sumIsThousand|> Seq.take 1|> Seq.map calcProduct|> Seq.exactlyOne|> printfn "%d"
